\documentclass[]{manual}

\title{DC Instruments and Measurements}
\date{\today}
\course{Physics 321}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section{Motivation}
    
    In this lab we will learn about some of the laboratory instruments we will use throughout the semester by using them to measure direct-current (DC) voltages and currents.
    We will also observe how measurement devices like voltmeters and ammeters can sometimes have non-negligible effects on the behavior of a circuit when attached.

    \section{Background}
    
    In this lab we will consider \textbf{DC voltage and current dividers}.
    These devices use \textbf{resistor networks} to provide a selected fraction of some input voltage or current as output.
    To determine that fraction, we need to apply the two basic rules that apply to all electrical circuits:
    \begin{itemize}
        \item The voltage across \textbf{parallel branches} of the circuit must be the same.
        \item The current across \textbf{components in series} must be the same.
    \end{itemize}
    
    \vspace{1em}
    
    Because we are working with resistors, you will also need \textbf{Ohm's Law}:
    \begin{align}
        V = IR
    \end{align}
    where $V$ is the voltage across some resistor, $I$ is the current flowing through that resistor, and $R$ is the resistance of the resistor.
    Quickly rearranging Ohm's Law to solve for different values is a critical skill.
    
    A \textbf{voltmeter} is a device which measures voltages.
    Conceptually, you can image a voltmeter as a resistor with extremely large and well-known resistance.
    The voltmeter is attached to the circuit between two points, just like it was some component.
    A tiny current then flows across the voltmeter, which is measured and used with the known resistance via Ohm's Law to calculate the voltage between the two points in the circuit.
    If the resistance is large enough, the current that flows across the voltmeter is small enough to not perturb the normal operation of the circuit.

    \section{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item Two multimeters.
        \item Two variable-resistance boxes.
        \item An incandescent light bulb filament.
    \end{itemize}

    \FloatBarrier
    \newpage

    \section{Procedure}
    
    \subsection{Measuring Voltage with Real Devices}
    
    Build the circuit shown in Figure~\ref{fig:voltage-divider-with-voltmeters} using the DC power supply and two resistance boxes.
    Do not actually attach any voltmeters yet, but make sure the connections you'll need are available.
    Set $V_0 = \SI{1}{\volt}$.
    
    Using the voltmeter (this will be one of the settings on your multimeter), measure $V_1$, $V_2$, and $V_{12}$ for $R_1 = R_2 = \SI{100}{\kilo\ohm}$, $R_1 = R_2 = \SI{1}{\mega\ohm}$, and $R_1 = R_2 = \SI{10}{\mega\ohm}$.
    \textbf{Do not attach more than one voltmeter at a time: measure each voltage independently.}
    
    If the voltmeter was \textbf{ideal} (i.e., no current flowed through it, as if it had infinite resistance), we would expect $V_1 + V_2 = V_{12}$, no matter how large $R_1$ and $R_2$ are.
    Make a table summarizing your results and reporting the percent difference between $V_1 + V_2$ and $V_{12}$.
    Does it depend on how large $R_1$ and $R_2$ are?
    
    Look up the input impedance of the voltmeter --- this is its effective resistance in the circuit.
    Can you explain your results quantitatively by including the voltmeters in the voltage divider calculation?
    
    \begin{figure}
        \centering
        
        \begin{circuitikz}
            \pgfmathsetmacro{\VoltageSourceX}{0};
            \pgfmathsetmacro{\ResistorsX}{2};
            \pgfmathsetmacro{\FirstProbesX}{4};
            \pgfmathsetmacro{\SecondProbeX}{6};
        
            \draw (\VoltageSourceX, 4) 
                to[V=$V_0$] (\VoltageSourceX, 0)
                -- (\ResistorsX, 0)
                to[R=$R_2$] (\ResistorsX, 2)
                to[R=$R_1$] (\ResistorsX, 4)
                -- (\VoltageSourceX, 4)
            ;
            
            \draw (\ResistorsX, 4) 
                -- (\FirstProbesX, 4)
                to[qvprobe, l=$V_1$] (\FirstProbesX, 2)
                -- (\ResistorsX, 2)
            ;
            
            \draw (\ResistorsX, 2) 
                -- (\FirstProbesX, 2)
                to[qvprobe, l=$V_2$] (\FirstProbesX, 0)
                -- (\ResistorsX, 0)
            ;
            
            \draw (\FirstProbesX, 4) 
                -- (\SecondProbeX, 4)
                to[qvprobe, l=$V_{12}$] (\SecondProbeX, 0)
                -- (\FirstProbesX, 0)
            ;
        \end{circuitikz}
        
        \caption{
            A simple voltage divider circuit, with voltmeters attached.
        }
        
        \label{fig:voltage-divider-with-voltmeters}
    \end{figure}
    
    \subsection{Compensating for Non-Ideal Measurement Devices}
    
    Although modern measurement devices are generally quite ideal for most circuits, it is important to understand what to do when this assumption fails.
    The circuits shown in Figure~\ref{fig:resistance-measurement-circuits} can be used to measure a resistance with non-ideal devices by measuring a current and voltage simultaneously.
    
    In Circuit A, the ammeter reads the current that passes through the resistor, as desired.
    The voltmeter, however, reads the voltage across both the resistor and the ammeter.
    
    In Circuit B, the voltmeter reads the voltage across the resistor, as desired.
    The ammeter, however, reads the current that passes through both the resistor and the voltmeter.
    
    Derive a relationship for each circuit that lets you calculate $R$ from a measurement of $V$ and $I$, given the internal resistances of the measurement devices, $R_V$ and $R_I$.
    You should draw a version of each circuit where the ideal measurement devices have been replaced by the equivalent non-ideal measurement devices (e.g., a non-ideal voltmeter is the internal resistance of that voltmeter in parallel with an ideal voltmeter).
    
    Build each circuit using a \SI{10}{\mega\ohm} resistor for $R$.
    Measure the resistance of the resistor using the relationships you derived above (look up the internal resistances, which may be called ``input impedances'', in datasheets), and compare to the expected value of \SI{10}{\mega\ohm}.
    How well did your measurement circuits work?
    
    \begin{figure}
        \centering
        
        \begin{subfigure}[b]{.45\textwidth}
            \centering
            
            \begin{circuitikz}
                \pgfmathsetmacro{\VoltageSourceX}{0};
                \pgfmathsetmacro{\ResistorX}{2};
                \pgfmathsetmacro{\ProbeX}{4};
            
                \draw (\VoltageSourceX, 4) 
                    to[V=$V_0$] (\VoltageSourceX, 0)
                    -- (\ResistorX, 0)
                    to[qiprobe, invert, l=$I$] (\ResistorX, 2)
                    to[R=$R$] (\ResistorX, 4)
                    -- (\VoltageSourceX, 4)
                ;
                
                \draw (\ResistorX, 4) 
                    -- (\ProbeX, 4)
                    to[qvprobe, l=$V$] (\ProbeX, 0)
                    -- (\ResistorX, 0)
                ;
            \end{circuitikz}
            
            \caption{Circuit A.}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{.45\textwidth}
            \centering
                        
            \begin{circuitikz}
                \pgfmathsetmacro{\VoltageSourceX}{0};
                \pgfmathsetmacro{\ResistorX}{2};
                \pgfmathsetmacro{\ProbeX}{4};
            
                \draw (\VoltageSourceX, 4) 
                    to[V=$V_0$] (\VoltageSourceX, 0)
                    -- (\ResistorX, 0)
                    to[qiprobe, invert, l=$I$] (\ResistorX, 2)
                    to[R=$R$] (\ResistorX, 4)
                    -- (\VoltageSourceX, 4)
                ;
                
                \draw (\ResistorX, 4)
                    -- (\ProbeX, 4)
                    to[qvprobe, l=$V$] (\ProbeX, 2)
                    -- (\ResistorX, 2)
                ;
            \end{circuitikz}
            
            \caption{Circuit B.}
        \end{subfigure}
        
        
        \caption{
            Circuits for measuring a resistance using non-ideal voltmeters and ammeters.
        }
        
        \label{fig:resistance-measurement-circuits}
    \end{figure}
    
    \subsection{Resistor Linearity}
    
    In the previous two sections, we investigated the possibility that our measurement devices were not behaving ideally.
    But how do we know whether the resistors we are measuring are behaving ideally?
    We have been assuming that Ohm's Law is always obeyed.
    But is that really true?
    
    Build a circuit to measure the voltage and current across a \SI{1}{\kilo\ohm} resistor.
    Measure the current at voltages from \SIrange{0}{20}{\volt} in \SI{1}{\volt} increments, and plot both $I$ vs. $V$ and $R = V/I$ vs. $V$.
    Does the resistor obey Ohm's Law (i.e., is it ``linear'')?
    
    Now perform the same experiment using an incandescent light bulb filament.
    Take a similar number of data points, but do not drive the filament with more than \SI{14}{\volt} or \SI{120}{\milli\ampere}.
    The brightness of the filament is an indication of its temperature.
    What happens to the temperature and resistance as the voltage increases?
    Does the filament obey Ohm's Law?

\end{document}
