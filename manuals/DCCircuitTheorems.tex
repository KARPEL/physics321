\documentclass[]{manual}

\title{DC Circuit Theorems}
\date{\today}
\course{Physics 321}

\begin{document}
    \maketitle
    \thispagestyle{firstpage}

    %%%

    \section{Motivation}
    
    We often don't know know exactly what is inside a circuit.
    However, we can still learn most of the important things about the circuit by measuring its behavior under controlled conditions.
    
    \section{Background}
    
    A circuit is \textbf{linear} if, when you apply a voltage $V$ at some port and measure the current $I$ at another port with all other ports shorted, $V/I$ is a constant as $V$ is varied (there are many equivalent definitions).
    For example, a single resistor $R$ is a linear circuit: if you apply a voltage $V$, a current $V/R = I$ flows across it.
    The ratio $V/I = R$ is therefore constant.
    Such a circuit obeys several useful theorems, some of which we will investigate in this lab: Thevenin's theorem, Norton's theorem, the Superposition theorem, and the Reciprocity theorem.
    
    \textbf{Thevenin and Norton's theorems} let us think of the inside of a ``black box'' circuit as a simple \textbf{equivalent circuit}, as shown in Figure~\ref{fig:equivalent-circuits}.
    The component values of the imaginary equivalent circuit can be determined by measuring the voltage across the port (for Thevenin, called the ``open-circuit voltage'' $V_{\mathrm{OC}}$ and $I_{\mathrm{SC}}$.
    If your measurement devices are ideal, the open-circuit voltage is $V_T$ and the short-circuit current is $I_N$.
    The resistances $R_T$ and $R_N$ turn out to be the same, and are both given by $R_T = R_N = V_T / I_N$.
    
    The \textbf{Superposition theorem} essentially says that inputs at each port produce outputs at the other ports independently of each other.
    
    The \textbf{Reciprocity theorem} says that the circuit is ``invertible'': if we send an input to a port and measure an output at some other port, we can switch the input and output ports and get the same output from the same input.
    
    \begin{figure}
        \centering
        
        \begin{subfigure}[b]{.45\textwidth}
            \centering
            
            \begin{circuitikz}
                \pgfmathsetmacro{\TerminalX}{4}
                \pgfmathsetmacro{\TopY}{3}
                \pgfmathsetmacro{\VSourceX}{0}
                \pgfmathsetmacro{\BottomY}{0}
                \pgfmathsetmacro{\BoxOffset}{1}
                
            
                \draw (\VSourceX, \TopY) 
                    to[V=$V_T$] (\VSourceX, \BottomY)
                    to[short, -o] (\TerminalX, \BottomY)
                ;
                
                \draw (\VSourceX, \TopY)
                    to[R=$R_T$, -o] (\TerminalX, \TopY)
                ;
                
                \draw[ultra thick, dashed] (\VSourceX - \BoxOffset, \BottomY - \BoxOffset)
                    -- (\TerminalX - \BoxOffset, \BottomY - \BoxOffset)
                    -- (\TerminalX - \BoxOffset, \TopY + \BoxOffset)
                    -- (\VSourceX - \BoxOffset, \TopY + \BoxOffset)
                    -- cycle
                ;
                
            \end{circuitikz}
            
            \caption{The Thevenin-equivalent circuit.}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{.45\textwidth}
            \centering
            
            \begin{circuitikz}
                \pgfmathsetmacro{\TerminalX}{4}
                \pgfmathsetmacro{\TopY}{3}
                \pgfmathsetmacro{\VSourceX}{0}
                \pgfmathsetmacro{\BottomY}{0}
                \pgfmathsetmacro{\BoxOffset}{1}
                \pgfmathsetmacro{\ResistorX}{2}
                
            
                \draw (\VSourceX, \BottomY) 
                    to[I=$I_N$] (\VSourceX, \TopY)
                    to[short, -o] (\TerminalX, \TopY)
                ;
                
                \draw (\VSourceX, \BottomY)
                    to[short, -o] (\TerminalX, \BottomY)
                ;
                
                \draw (\ResistorX, \TopY)
                    to[R=$R_N$] (\ResistorX, \BottomY)
                ;
                
                \draw[ultra thick, dashed] (\VSourceX - \BoxOffset, \BottomY - \BoxOffset)
                    -- (\TerminalX - \BoxOffset, \BottomY - \BoxOffset)
                    -- (\TerminalX - \BoxOffset, \TopY + \BoxOffset)
                    -- (\VSourceX - \BoxOffset, \TopY + \BoxOffset)
                    -- cycle
                ;
            \end{circuitikz}
            
            \caption{The Norton-equivalent circuit.}
        \end{subfigure}
        
        
        \caption{
            Thevenin and Norton-equivalent circuits.
            The inside of the dashed box is really some complicated linear DC circuit, but from the outside of the box it appears to be either a single voltage source in series with a single resistor (Thevenin) or a single current source in parallel with a single resistor (Norton).
            Any measurements made at the terminals on the right side of each box will behave as if the inside of the box is either of the equivalent circuits.
        }
        
        \label{fig:equivalent-circuits}
    \end{figure}
    
    Your tool for investigating these theorems is a ``black box'' circuit with three external ports.
    Each port is a pair of terminals, connected through the outside of the box just like the terminal pairs in Figure~\ref{fig:equivalent-circuits} are.
    \textit{
    The various DC circuit theorems will only hold if all unused ports of the box are shorted.
    Throughout the entire procedure below, make sure the unused ports are shorted!
    }
    The box also has a switch on it with two modes: \textbf{R} and \textbf{L}.
    By investigating the behavior of the box in these two modes, you will determine in which mode the circuit is linear, and eventually, what that nonlinear component is.
    
    \section{Equipment}

    For this lab, you will need:
    \begin{itemize}
        \item A DC power supply with two independent outputs.
        \item A voltmeter and an ammeter.
        \item A ``black box'' circuit.
        \item A \SI{100}{\kilo\ohm} resistance box.
    \end{itemize}

    \FloatBarrier
    \newpage

    \section{Procedure}
    
    \subsection{Thevenin's Theorem}
    
    With the box's switch in the \textbf{R} position, short port \#2 and apply a variable voltage $V_1$ at port \#1.
    Measure the current $I_3$ that appears at port \#3 for $V_1$ ranging from \SI{0}{\volt} to \SI{20}{\volt} in \SI{2}{\volt} steps.
    Plot $I_3$ vs. $V_1$.
    Is the circuit \textbf{linear}?
    Does Thevenin's theorem apply?
    
    Do the same measurements and answer the same questions with the box's switch in the \textbf{L} position.
    
    \subsection{Thevenin and Norton Parameters} 
    
    Switch the box back to \textbf{R}.
    We will now determine $V_T$, $R_T$, and $I_N$ for port \#3 being driven by port \#1 via a variety of different methods.
    
    \subsubsection{Open-Circuit Voltage and Short-Circuit Current} \label{sec:after-open-box-return-here}
    
    Set $V_1 = \SI{20}{\volt}$.
    Measure the open-circuit voltage $V_{\mathrm{OC}}$ and the short-circuit current $I_{\mathrm{SC}}$ at port \#3.
    Based on your measurements and Thevenin's and Norton's theorems, calculate $V_T$, $R_T$, and $I_N$.
    
    \subsubsection{Linearity}
    
    Insert a variable resistor $R$ in series with an ammeter at port \#3.
    Measure the total voltage $V$ across $R$ and the ammeter as a function, as well as the ammeter current $I$, as a function of $R$.
    Take about ten measurements that are roughly evenly-spaced in $V$.
    Make a plot of $I$ vs. $V$, and determine $R_T$ from the slope of this line.
    
    \subsubsection{Output Impedance}
    
    Remove the ammeter (so that port \#3 just has the resistor $R$ connecting its terminals).
    Vary $R$ until the voltage across it is $V_{\mathrm{OC}} / 2$.
    This value of $R$ is $R_T$, because we have essentially set up a 50/50 voltage divider between the box and $R$.
    
    \subsubsection{Direct Resistance Measurement}
    
    Disconnect the voltage source from port \#1 and short its terminals.
    Disconnect the resistor $R$.
    Directly measure the resistance of port \#3, which should also be $R_T$ by Thevenin's theorem.
    
    Compare the four measurements of $R_T$, assuming the direct resistance measurement to be the most accurate.
    
    \subsection{Superposition Theorem}
    
    If the circuit is linear, it should obey the Superposition theorem.
    For example, if we apply voltages at ports \#2 and \#3, the current we observe at port \#1 should be the sum of the currents we would observe with those voltages applied independently.
    
    Simultaneously apply $V_2 = \SI{20}{\volt}$ and $V_3 = \SI{15}{\volt}$, then measure the current at port \#1.
    Then apply the input voltages independently and measure the independent currents (remember to short-circuit the disconnected ports).
    Does the Superposition theorem hold for this circuit?
    
    Repeat the above with the switch in the \textbf{L} position.
    Does Superposition hold now?
    
    \subsection{$G$ Parameters and Reciprocity}
    
    Because the circuit obeys Superposition, we can determine all of the currents if we are given all of the voltages, like so:
    \begin{align} \label{eqn:g-parameters}
        I_1 &= G_{11} \, V_1 + G_{12} \, V_2 + G_{13} \, V_3 \\
        I_2 &= G_{21} \, V_1 + G_{22} \, V_2 + G_{23} \, V_3 \nonumber \\
        I_3 &= G_{31} \, V_1 + G_{32} \, V_2 + G_{33} \, V_3 \nonumber
    \end{align}
    Our goal is to measure all nine ``$G$ parameters'' ($G_{11}$, $G_{12}$, etc.).
    
    To measure a specific $G$ parameter $G_{ij}$, short the port that isn't $i$ or $j$ (so that port's voltage is zero), connect an ammeter to port $i$ (so that $V_i$ is also zero), and apply a voltage to port $j$.
    For example, suppose we want to measure $G_{12}$.
    The relevant equation is the first line of \eqref{eqn:g-parameters},
    \begin{align}
        I_1 &= G_{11} \, V_1 + G_{12} \, V_2 + G_{13} \, V_3
    \end{align}
    We short port \#3, so $V_3$ is zero:
    \begin{align}
        I_1 &= G_{11} \, V_1 + G_{12} \, V_2
    \end{align}
    Then we put an ammeter across port \#1, so $V_1$ is also zero and we can measure $I_1$:
    \begin{align}
        I_1 &= G_{12} \, V_2
    \end{align}
    Now we just need to apply a known voltage $V_2$, measure $I_1$, and rearrange to calculate $G_{12}$.
    
    Measure all nine $G$ parameters of the box with the switch in the \textbf{R} position.
    If the circuit is linear, it should obey the Reciprocity theorem, which says that if put in a voltage at some port and measure a current at another port, we should be able to get the same current at the first port by putting the same voltage into the second port.
    If you work through the math, you will find that this implies that $G_{ij} = G_{ji}$.
    Do your measurements indicate that the circuit obeys the Reciprocity theorem?
    
    \subsection{Opening the Box}
    
    Now that we know everything about the box's behavior, its time to look inside.
    Open the box and make a schematic diagram of the circuit when the switch is in the \textbf{R} position.
    Calculate $V_T$, $R_T$, and $I_N$ for the circuit you used in Section \ref{sec:after-open-box-return-here}.
    Compare those measurements to the values you just calculated.
    
    Look for the nonlinear component in the circuit.
    What is it, and why is it nonlinear?

\end{document}
